default:
  interruptible: true

include:
  - local: '.windows-gitlab-ci.yml'
  - project: 'tango-controls/gitlab-ci-templates'
    file: 'ArchiveWithSubmodules.gitlab-ci.yml'

variables: &variables
  DOCKER_HOST: tcp://docker:2375
  DOCKER_TLS_CERTDIR: ""
  # Build parameters. We later override them at job level if needed.
  CMAKE_DISABLE_PRECOMPILE_HEADERS: "OFF"
  CMAKE_BUILD_TYPE: "Debug"
  TANGO_USE_JPEG: "ON"
  BUILD_SHARED_LIBS: "ON"
  TANGO_USE_LIBCPP: "OFF"
  TOOLCHAIN_FILE: ""
  CTEST_PARALLEL_LEVEL: "1"
  BUILD_TESTING: "ON"
  TANGO_USE_TELEMETRY: "ON"
  # docker image names and versions
  DEBIAN_12_IMAGE: "debian12:v7"
  DEBIAN_12_CROSS_IMAGE: "debian12-cross-32bit:v6"
  DEBIAN_11_IMAGE: "debian11:v6"
  UBUNTU_20_IMAGE: "ubuntu-20.04:v7"
  UBUNTU_20_JPEG9_IMAGE: "ubuntu-20.04-jpeg9:v6"
  LLVM_LATEST_IMAGE: "llvm-latest:v10"
  GCC_LATEST_IMAGE: "gcc-latest:v6"
  FEDORA_IMAGE: "fedora37:v6"
  ALPINE_IMAGE: "alpine-3.15:v6"
  DEBIAN_MINIMUM_VERSIONS_IMAGE: "debian-minimum-versions:v6"
  DEBIAN_MAXIMUM_CMAKE_IMAGE: "debian-maximum-cmake:v6"
  GIT_SUBMODULE_STRATEGY: "recursive"

# See: https://docs.gitlab.com/ce/ci/yaml/README.html#workflowrules-templates
workflow:
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

pre-commit:
  image: registry.gitlab.com/tango-controls/docker/pre-commit:3.0.0
  services: []
  tags:
    - docker, linux, amd64
  variables:
    PRE_COMMIT_HOME: ${CI_PROJECT_DIR}/.cache/pre-commit
    PATCH_FILE: clang-format.patch
  script:
    - pre-commit run --all-files --show-diff-on-failure || true
    - git diff > ${PATCH_FILE}
      # fail job if the patch file is not empty
    - '! test -s ${PATCH_FILE}'
  cache:
    paths:
      - ${PRE_COMMIT_HOME}
  artifacts:
    when: always
    paths:
      - ${PATCH_FILE}

.job-template-no-test-run: &job-template-no-test-run
  image: registry.gitlab.com/tango-controls/docker/ci/cpptango/${OS_TYPE}
  services: []
  tags:
    - docker, linux, amd64
  needs:
    - job: pre-commit
      artifacts: false
  script:
    - ci/config.sh
    - ci/build.sh

.job-template-with-tests: &job-template-with-tests
  image: registry.gitlab.com/tango-controls/docker/ci/cpptango/${OS_TYPE}
  services:
    - docker:20.10.16-dind
  tags:
    - dind, skao, docker, linux, amd64
  needs:
    - job: pre-commit
      artifacts: false
  script:
    - ci/config.sh
    - ci/build.sh
    - ci/test.sh
  after_script:
    - ci/print_coredumps.sh
    - mkdir -p build/tests/results
  artifacts:
    when: always
    paths:
      - build/tests/results
      - build/tests/core.*

abi-api-compliance-check:
  <<: *job-template-no-test-run
  variables:
    <<: *variables
    OS_TYPE: "${GCC_LATEST_IMAGE}"
    GIT_COMMITTER_NAME: "$GITLAB_USER_NAME"
    GIT_COMMITTER_EMAIL: "$GITLAB_USER_EMAIL"
    CI_TARGET_BRANCH: "${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}"
    GIT_STRATEGY: "none"
    TANGO_USE_TELEMETRY: "OFF"
  rules:
    # This job runs only for merge requests.
    - if: $CI_MERGE_REQUEST_IID
  # allow failures for next release cycle which is allowed to break ABI
  allow_failure: true
  script: # override
    - git clone -b ${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME} ${CI_PROJECT_URL} src
    - cd src
    - ci/check-ABI-API-compliance.sh
  artifacts:
    when: always
    paths:
      - src/compat_reports

llvm-latest:
  <<: *job-template-no-test-run
  variables:
    <<: *variables
    OS_TYPE: "${LLVM_LATEST_IMAGE}"
    TANGO_WARNINGS_AS_ERRORS: "ON"
    CMAKE_DISABLE_PRECOMPILE_HEADERS: "ON"

gcc-latest:
  <<: *job-template-no-test-run
  parallel:
    matrix:
      - cxx: [17, 20, 23]
  variables:
    <<: *variables
    OS_TYPE: "${GCC_LATEST_IMAGE}"
    TANGO_WARNINGS_AS_ERRORS: "ON"
    CMAKE_DISABLE_PRECOMPILE_HEADERS: "ON"
    CMAKE_CXX_STANDARD: "${cxx}"
    TANGO_USE_TELEMETRY: "OFF"

ubuntu-20.04:
  <<: *job-template-with-tests
  variables:
    <<: *variables
    OS_TYPE: "${UBUNTU_20_IMAGE}"

coverage:
  <<: *job-template-with-tests
  variables:
    <<: *variables
    TANGO_ENABLE_COVERAGE: "ON"
    OS_TYPE: "${DEBIAN_12_IMAGE}"
  after_script:
    - mkdir -p build/tests/results
    - mkdir coverage
    - >
        gcovr --gcov-ignore-parse-errors=negative_hits.warn_once_per_file
        --filter '^src/' --filter '^log4tango/(?!tests/)' -j$(nproc)
        --xml --output coverage.xml
    - >
        gcovr --gcov-ignore-parse-errors=negative_hits.warn_once_per_file
        --filter '^src/' --filter '^log4tango/(?!tests/)' -j$(nproc)
        --html-details --output coverage/coverage.html
    - >
        gcovr --gcov-ignore-parse-errors=negative_hits.warn_once_per_file
        --filter '^src/' --filter '^log4tango/(?!tests/)' -j$(nproc)
    - tar czf coverage.tar.gz coverage
  artifacts:
    when: always
    reports:
      coverage_report:
        # coverage report provides line-by-line info
        coverage_format: cobertura
        path: coverage.xml
    paths:
      - build/tests/results
      - build/tests/core.*
      - coverage.xml
      - coverage.tar.gz
  # keyword/regex to extract total coverage % for this CI job (which is also called "coverage")
  coverage: '/^TOTAL.*\s+(\d+\%)$/'

debian12-static:
  <<: *job-template-with-tests
  variables:
    <<: *variables
    OS_TYPE: "${DEBIAN_12_IMAGE}"
    BUILD_SHARED_LIBS: "OFF"

debian12-no-pch:
  <<: *job-template-no-test-run
  variables:
    <<: *variables
    OS_TYPE: "${DEBIAN_12_IMAGE}"
    CMAKE_DISABLE_PRECOMPILE_HEADERS: "ON"
    TANGO_WARNINGS_AS_ERRORS: "ON"

debian12-release:
  <<: *job-template-with-tests
  variables:
    <<: *variables
    OS_TYPE: "${DEBIAN_12_IMAGE}"
    CMAKE_BUILD_TYPE: Release
    TANGO_WARNINGS_AS_ERRORS: "ON"

debian12-release-no-pch:
  <<: *job-template-with-tests
  variables:
    <<: *variables
    OS_TYPE: "${DEBIAN_12_IMAGE}"
    CMAKE_BUILD_TYPE: Release
    CMAKE_DISABLE_PRECOMPILE_HEADERS: "ON"
    TANGO_WARNINGS_AS_ERRORS: "ON"

debian12:
  <<: *job-template-with-tests
  variables:
    <<: *variables
    OS_TYPE: "${DEBIAN_12_IMAGE}"

debian12-no-submodule-init:
  <<: *job-template-no-test-run
  variables:
    <<: *variables
    OS_TYPE: "${DEBIAN_12_IMAGE}"
  script:
    - git submodule deinit TangoCMakeModules
    - ci/config.sh

debian12-no-jpeg:
  <<: *job-template-with-tests
  variables:
    <<: *variables
    OS_TYPE: "${DEBIAN_12_IMAGE}"
    TANGO_USE_JPEG: "OFF"

debian12-no-telemetry:
  <<: *job-template-with-tests
  variables:
    <<: *variables
    OS_TYPE: "${DEBIAN_12_IMAGE}"
    TANGO_USE_TELEMETRY: "OFF"

debian12-cross-32bit:
  <<: *job-template-no-test-run
  variables:
    <<: *variables
    OS_TYPE: "${DEBIAN_12_CROSS_IMAGE}"
    CMAKE_BUILD_TYPE: "Debug"
    TOOLCHAIN_FILE: "configure/toolchain-i686.cmake"
    CMAKE_DISABLE_PRECOMPILE_HEADERS: "ON"
    TANGO_WARNINGS_AS_ERRORS: "ON"
    TANGO_USE_JPEG: "OFF"
    TANGO_USE_TELEMETRY: "OFF"

debian11:
  <<: *job-template-with-tests
  variables:
    <<: *variables
    OS_TYPE: "${DEBIAN_11_IMAGE}"

ubuntu-20.04-jpeg9:
  <<: *job-template-with-tests
  variables:
    <<: *variables
    OS_TYPE: "${UBUNTU_20_JPEG9_IMAGE}"

clang-analyzer:
  <<: *job-template-no-test-run
  variables:
    <<: *variables
    OS_TYPE: "${UBUNTU_20_IMAGE}"
    CMAKE_EXPORT_COMPILE_COMMANDS: "ON"
    CMAKE_DISABLE_PRECOMPILE_HEADERS: "ON"
    BUILD_TESTING: "OFF"
    CMAKE_BUILD_TYPE: "Debug"
    TANGO_MAKE_TARGET: "idl_source"
    CC: /usr/lib/llvm-12/bin/clang
    CXX: /usr/lib/llvm-12/bin/clang++
  script: # override
    - sudo ln -s /usr/bin/clang-12 /usr/local/bin/clang
    - sudo ln -s /usr/bin/clang-extdef-mapping-12 /usr/local/bin/clang-extdef-mapping
    - ci/config.sh
    - ci/build.sh
    # scan-build command must be wrapped in quotes because
    # --analyzer-config option cannot contain whitespaces.
    - "/usr/share/clang/scan-build-py-12/bin/analyze-build                          \
      -v                                                                            \
      --cdb build/compile_commands.json                                             \
      --exclude build/cppapi/server/idl                                             \
      --keep-empty                                                                  \
      --output clang-analyzer-results                                               \
      --force-analyze-debug-code                                                    \
      --analyzer-config                                                             \
        stable-report-filename=true,aggressive-binary-operation-simplification=true \
      --enable-checker core                                                         \
      --enable-checker cplusplus                                                    \
      --enable-checker deadcode                                                     \
      --enable-checker nullability                                                  \
      --enable-checker optin.cplusplus                                              \
      --enable-checker optin.performance                                            \
      --enable-checker optin.portability                                            \
      --enable-checker security                                                     \
      --enable-checker unix                                                         \
      --enable-checker alpha.clone                                                  \
      --enable-checker alpha.core                                                   \
      --enable-checker alpha.cplusplus                                              \
      --enable-checker alpha.deadcode                                               \
      --enable-checker alpha.nondeterminism                                         \
      --enable-checker alpha.unix                                                   \
      > clang-analyzer-output.txt"
  artifacts:
    when: always
    paths:
      - clang-analyzer-output.txt
      - clang-analyzer-results

clang-tidy:
  <<: *job-template-no-test-run
  variables:
    <<: *variables
    OS_TYPE: "${LLVM_LATEST_IMAGE}"
    CMAKE_EXPORT_COMPILE_COMMANDS: "ON"
    CMAKE_UNITY_BUILD: "ON"
    CMAKE_DISABLE_PRECOMPILE_HEADERS: "ON"
    BUILD_TESTING: "OFF"
    CMAKE_BUILD_TYPE: "Debug"
    TANGO_MAKE_TARGET: "idl_source"
  script: # override
    - ci/config.sh
    - ci/build.sh
    - set -o pipefail
    - >
      ${RUN_CLANG_TIDY}
      -p build -header-filter='.*' -config-file=ci/report.clang-tidy 'src' 'log4tango/src' 'tests/catch2*'
      | sed 's/\x1B\[[0-9;]\{1,\}[A-Za-z]//g' > clang-tidy-output.txt
  after_script:
    - set +o pipefail
    # Print error summary (check name and occurrence count).
    - >
      grep -E 'error: .+ \[.+\]$' clang-tidy-output.txt
      | sort | uniq | sed -E 's|^.+ \[(.+)\]|\1|' | sort | uniq -c
    # Print warning summary (check name and occurrence count).
    - >
      grep -E 'warning: .+ \[.+\]$' clang-tidy-output.txt
      | sort | uniq | sed -E 's|^.+ \[(.+)\]|\1|' | sort | uniq -c
    # Produce a report in Code Climate JSON format.
    - >
      cat clang-tidy-output.txt
      | ./ci/clang-tidy-to-code-climate.py "$(pwd)/"
      > code-quality-report.json
    # Produce a report in HTML format.
    - >
      cat code-quality-report.json
      | ruby -I/home/tango/dependencies/codeclimate/lib ci/code-climate-json-to-html.rb "$(pwd)"
      > code-quality-report.html
  artifacts:
    when: always
    reports:
       codequality: code-quality-report.json
    paths:
      - clang-tidy-output.txt
      - code-quality-report.json
      - code-quality-report.html

# job to build against custom library locations and minimum supported versions of dependent libraries
# using <PackageName>_ROOT
cmake-with-locations-and-min-versions-ROOT:
  <<: *job-template-no-test-run
  variables:
    <<: *variables
    OS_TYPE: "${DEBIAN_MINIMUM_VERSIONS_IMAGE}"
  parallel:
    matrix:
      - BUILD_TYPE: [Release, Debug, RelWithDebInfo, MinSizeRel]
  script:
    # needs to be in-sync with scripts.git/install_minimum_versions.sh
    - INSTALL_PREFIX=/tmp/install
    - export PATH=${PATH}:${INSTALL_PREFIX}/cmake/bin
    - >
      cmake
      -Werror=dev
      -B build
      -DCMAKE_BUILD_TYPE=${BUILD_TYPE}
      -DCatch2_ROOT=${INSTALL_PREFIX}/catch
      -Dcppzmq_ROOT=${INSTALL_PREFIX}/cppzmq
      -Dtangoidl_ROOT=${INSTALL_PREFIX}/tango-idl
      -DomniORB4_ROOT=${INSTALL_PREFIX}/omniORB
      -DZeroMQ_ROOT=${INSTALL_PREFIX}/libzmq
      -DJPEG_ROOT=${INSTALL_PREFIX}/libjpeg
      -DTANGO_USE_TELEMETRY=ON
      -Dabsl_ROOT=${INSTALL_PREFIX}/abseil
      -DProtobuf_ROOT=${INSTALL_PREFIX}/protobuf
      -DgRPC_ROOT=${INSTALL_PREFIX}/gRPC
      -Dopentelemetry-cpp_ROOT=${INSTALL_PREFIX}/opentelemetry
    - ci/build.sh

# and pkg-config/CMAKE_PREFIX_PATH
cmake-with-locations-and-min-versions-env:
  <<: *job-template-no-test-run
  variables:
    <<: *variables
    OS_TYPE: "${DEBIAN_MINIMUM_VERSIONS_IMAGE}"
  parallel:
    matrix:
      - BUILD_TYPE: [Release, Debug, RelWithDebInfo, MinSizeRel]
  script:
    # needs to be in-sync with scripts.git/install_minimum_versions.sh
    - INSTALL_PREFIX=/tmp/install
    - export PATH=${PATH}:${INSTALL_PREFIX}/cmake/bin
    - export PKG_CONFIG_PATH="${INSTALL_PREFIX}/omniORB/lib/pkgconfig:${INSTALL_PREFIX}/libjpeg/lib/pkgconfig:${INSTALL_PREFIX}/tango-idl/lib/pkgconfig:${INSTALL_PREFIX}/libzmq/lib/pkgconfig"
    - export CMAKE_PREFIX_PATH="${INSTALL_PREFIX}/catch:${INSTALL_PREFIX}/abseil/lib/cmake:${INSTALL_PREFIX}/protobuf/lib/cmake:${INSTALL_PREFIX}/gRPC/lib/cmake:${INSTALL_PREFIX}/opentelemetry/lib/cmake"
    - >
      cmake
      -Werror=dev
      -B build
      -DCMAKE_BUILD_TYPE=${BUILD_TYPE}
      -Dcppzmq_ROOT=${INSTALL_PREFIX}/cppzmq
      -DTANGO_USE_JPEG=ON
      -DJPEG_ROOT=${INSTALL_PREFIX}/libjpeg
      -DTANGO_USE_TELEMETRY=ON
    - ci/build.sh

debian-maximum-cmake-and-presets:
  <<: *job-template-no-test-run
  variables:
    <<: *variables
    OS_TYPE: "${DEBIAN_MAXIMUM_CMAKE_IMAGE}"
  parallel:
    matrix:
      - config: [rel, debug]
  script: # override
    - cmake --preset=ci-${config}-linux
    - cmake --build --preset=ci-${config}-linux

alpine:
  <<: *job-template-no-test-run
  variables:
    <<: *variables
    OS_TYPE: "${ALPINE_IMAGE}"

doxygen-documentation:
  <<: *job-template-no-test-run
  variables:
    <<: *variables
    OS_TYPE: "${DEBIAN_12_IMAGE}"
    TANGO_USE_JPEG: "OFF"
    BUILD_TESTING: "OFF"
    TANGO_MAKE_TARGET: "doc"
    TANGO_USE_TELEMETRY: "OFF"
  artifacts:
    when: always
    paths:
      - build/doc_html

sanitizer:
  <<: *job-template-with-tests
  variables:
    <<: *variables
    OS_TYPE: "${UBUNTU_20_IMAGE}"
    CC: /usr/lib/llvm-12/bin/clang
    CXX: /usr/lib/llvm-12/bin/clang++
    ASAN_OPTIONS: detect_stack_use_after_return=1
    TSAN_OPTIONS: second_deadlock_stack=1
    UBSAN_OPTIONS: "\
      print_stacktrace=1,\
      report_error_type=1,\
      halt_on_error=1,\
      suppressions=$CI_PROJECT_DIR/tests/suppressions-ubsan.supp"
    SKIP_TESTS: "(event::per_event|old_tests::ring_depth)"
    CMAKE_DISABLE_PRECOMPILE_HEADERS: "ON"
  # Increase timeout, this is required by UBSAN job.
  timeout: 2h
  parallel:
    matrix:
      - TANGO_ENABLE_SANITIZER: [ASAN, TSAN, UBSAN]
  rules:
    - if: '$CI_JOB_NAME == "sanitizer: [UBSAN]"'
      allow_failure: false
      when: on_success
    # Failures are temporarily allowed until TSAN and ASAN sanitizer issues are fixed.
    - allow_failure: true
      when: on_success

# Trigger conda dev package build for the default branch
build-conda-dev-package:
  stage: test
  variables:
    DEPLOY_PACKAGE: "true"
  trigger:
    project: tango-controls/conda/cpptango-feedstock
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

libcpp:
  <<: *job-template-no-test-run
  variables:
    <<: *variables
    OS_TYPE: "${LLVM_LATEST_IMAGE}"
    TANGO_WARNINGS_AS_ERRORS: "ON"
    CMAKE_DISABLE_PRECOMPILE_HEADERS: "ON"
    TANGO_USE_LIBCPP: "ON"

pkgconfig-fedora37:
  <<: *job-template-no-test-run
  variables:
    <<: *variables
    OS_TYPE: "${FEDORA_IMAGE}"
    TANGO_USE_JPEG: "OFF"
    BUILD_TESTING: "OFF"
  script: # override
    - ci/config.sh
    - ci/run-pkg-config-validation.sh

cmake-superproject:
  <<: *job-template-no-test-run
  variables:
    <<: *variables
    OS_TYPE: "${DEBIAN_12_IMAGE}"
    TANGO_USE_TELEMETRY: "OFF"
  script: # override
    - cd tests/superproject
    - ../../ci/config.sh
    - ../../ci/build.sh
